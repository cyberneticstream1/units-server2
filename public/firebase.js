// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from "firebase/firestore";


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDCR8eInTmio5pBhmrp1EwPrO2MpWm0e5M",
    authDomain: "cybernetic-stream-37edf.firebaseapp.com",
    projectId: "cybernetic-stream-37edf",
    storageBucket: "cybernetic-stream-37edf.appspot.com",
    messagingSenderId: "582106675005",
    appId: "1:582106675005:web:3fe94fb7e071242fe83754"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore()